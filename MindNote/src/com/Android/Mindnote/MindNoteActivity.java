package com.Android.Mindnote;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MindNoteActivity extends Activity {
	
	
	private Button newNote;
	private Button allNotes;
	private Button search;
	private Button tags;
	protected Context context;
	public int flagNum,flagPasswordNum;
	private Bundle bl ; 
	private SharedPreferences passwordFlagSP;
	
	
	/*static final private int MENU_ITEM = Menu.FIRST;*/
	
	private String flag;
	private String answerText, ansText;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        this.setContentView(R.layout.main);
        
        this.newNote = (Button) findViewById(R.id.newnote);
        this.allNotes = (Button) findViewById(R.id.notes);
        this.search = (Button) findViewById(R.id.search);
        this.tags= (Button) findViewById(R.id.tags);
        
        
        
        
    	passwordFlagSP= getSharedPreferences(password.KEY_passwordFlag_PRIVATE, Context.MODE_PRIVATE);
    	flagPasswordNum=passwordFlagSP.getInt(password.KEY_passwordFlag_PRIVATE, 0);
    	
      /* if (flagPasswordNum==1) {
    	   Intent intent = new Intent();
			intent.setClass(MindNoteActivity.this, start.class);
			startActivity(intent);
	}else if(flagPasswordNum==0){
		Intent intent = new Intent();
		intent.setClass(MindNoteActivity.this, password.class);
		startActivity(intent);
	}*/
        
        
        newNote.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(MindNoteActivity.this, mindNoteEdit.class);
				startActivity(intent);
			}
		});

        allNotes.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setClass(MindNoteActivity.this, mindNoteList.class);
				bl = new Bundle();  
				flagNum=7;
				bl.putInt("flag", flagNum);
				intent.putExtras(bl); 
				
				startActivity(intent);
			}
		});
        
        search.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 Intent intent = new Intent();
                 intent.setClass(MindNoteActivity.this, tags.class);
                 bl = new Bundle();  
                 flagNum=0;                   
                 bl.putInt("flag", flagNum);
                 intent.putExtras(bl); 
                 startActivity(intent);
			}
		});

        tags.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 Intent intent = new Intent();
                 intent.setClass(MindNoteActivity.this, tags.class);
                 bl = new Bundle();  
                 flagNum=1;
                 
                 bl.putInt("flag", flagNum);
                 intent.putExtras(bl); 
                 startActivity(intent);
			}
		});
        
    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		int menuItemOrder = Menu.NONE;
		
		menu.add(0,1,menuItemOrder,"更改密码 ");
		/*menu.add(0,2,menuItemOrder,"重置密码");*/
		menu.add(0,3,menuItemOrder,"关于");
		return super.onCreateOptionsMenu(menu);
	}
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case 1:
			Editor passwordFlagSPEditor = passwordFlagSP.edit();
			passwordFlagSPEditor.putInt(password.KEY_passwordFlag_PRIVATE, 2);
			passwordFlagSPEditor.commit();
			Intent intent = new Intent();
    		intent.setClass(MindNoteActivity.this, start.class);
    		startActivity(intent);
			break;
		/*case 2:
			
			Editor passwordFlagSPEditor1 = ApasswordFlagSP.edit();
			passwordFlagSPEditor1.putInt(password.KEY_passwordFlag_PRIVATE, 3);
			passwordFlagSPEditor1.commit();
			SharedPreferences question = getSharedPreferences(password.spQuestion_PRIVATE, Context.MODE_PRIVATE);
			String questionText=question.getString(password.KEY_spQuestion_PRIVATE, null);
			SharedPreferences answer = getSharedPreferences(password.spAnswer_PRIVATE, Context.MODE_PRIVATE);
			answerText=answer.getString(password.KEY_spAnswer_PRIVATE, null);
			
			LayoutInflater factory = LayoutInflater.from(this);
			final View textEntryView = factory.inflate(R.layout.answer, null);
			
			AlertDialog.Builder adb = new Builder(MindNoteActivity.this);
			adb.setTitle("重置密码");
			adb.setMessage(questionText);
			adb.setView(textEntryView);
			
			
			adb.setPositiveButton("保存",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int which) {
							EditText ansTe = (EditText)textEntryView.findViewById(R.id.answer);
							ansText=ansTe.getText().toString();
							if (ansText.equals(answerText)) {
								Intent intent = new Intent();
					    		intent.setClass(MindNoteActivity.this, password.class);
					    		startActivity(intent);
							}
							else{
								Toast.makeText(MindNoteActivity.this, "密码错误，请重新输入", Toast.LENGTH_SHORT)
								.show();
							}
						}
					});
			adb.show();
			break;*/
		case 3:
			AlertDialog.Builder adb1=new Builder(MindNoteActivity.this);
			adb1.setTitle("关于我们");
			adb1.setMessage("杭州电子科技大学\n\t  严博制作 \n\t吕秋云老师指导");			
			adb1.show();
			break;
		default:
			break;
		}
		return super.onMenuItemSelected(featureId, item);
	}
	
}