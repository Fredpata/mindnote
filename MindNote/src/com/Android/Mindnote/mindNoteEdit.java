
package com.Android.Mindnote;

import com.Android.mindNoteDB.MindNoteDB;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * @author yanboeva
 *
 */
public class mindNoteEdit extends Activity{
	private EditText text_Title;
	private EditText text_Content;
	private EditText text_Tag;
	private Button button_Save;
	private Button button_Cancel;
	private MindNoteDB mDbHelper;
	private Long mRowId;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mDbHelper = new MindNoteDB(this);
		mDbHelper.open();
		
		this.setContentView(R.layout.note_edit);		
		this.setTitle(R.string.edit_note);
		
		this.button_Save = (Button) findViewById(R.id.button_Save);
	    this.button_Cancel = (Button) findViewById(R.id.button_Cancel);
	    this.text_Title = (EditText) findViewById(R.id.text_Title);
	    this.text_Content= (EditText) findViewById(R.id.text_Content);
	    this.text_Tag= (EditText) findViewById(R.id.text_Tag);
	    
	    mRowId = (savedInstanceState == null) ? null :
            (Long) savedInstanceState.getSerializable(MindNoteDB.KEY_ROWID);
		if (mRowId == null) {
			Bundle extras = getIntent().getExtras();
			mRowId = extras != null ? extras.getLong(MindNoteDB.KEY_ROWID)
									: null;
		}

		populateFields();
		
		button_Save.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				AlertDialog.Builder adb = new Builder(mindNoteEdit.this);
				adb.setTitle("保存");
				adb.setMessage("确定要保存?");
				adb.setPositiveButton("保存",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								saveState();
								finish();
							}
						});
				adb.setNegativeButton("取消",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								Toast.makeText(mindNoteEdit.this, "不保存",
										Toast.LENGTH_SHORT).show();
							}
						});
				adb.show();
			}			
			
		});
		button_Cancel.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.exit(0);
			}			
		});
	}
	private void populateFields(){
		if (mRowId != null) {
            Cursor note = mDbHelper.fetchNote(mRowId);
            startManagingCursor(note);
            text_Title.setText(note.getString(
                    note.getColumnIndexOrThrow(MindNoteDB.NOTE_TITLE)));
            text_Content.setText(note.getString(
                    note.getColumnIndexOrThrow(MindNoteDB.NOTE_CONTENT)));
            text_Tag.setText(note.getString(
                    note.getColumnIndexOrThrow(MindNoteDB.NOTE_TAG)));
        }
	}
	
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putSerializable(MindNoteDB.KEY_ROWID, mRowId);
	}


	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		saveState();
	}



	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		populateFields();
	}


	private void saveState(){
		 String title = text_Title.getText().toString();
	     String content = text_Content.getText().toString();
	     String tags = text_Tag.getText().toString();
	     
	     if ("".equals(title) || "".equals(content)) {
	    	 text_Title.setError("不能为空");
	         text_Content.setError("不能为空");
			} else if ("Tilte".equals(title) || "Content".equals(content)) {
				Toast.makeText(this, "请输入内容", Toast.LENGTH_SHORT).show();
			} else {   if (mRowId == null) {
		         long id = mDbHelper.createNote(title, content,tags);
		         if (id > 0) {
		              mRowId = id;
		         }
		     } else {
		         mDbHelper.updateNote(mRowId, title, content,tags);
		     }
		   }
	     
	   	}
	  


}
