package com.Android.Mindnote;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import com.Android.mindNoteDB.MindNoteDB;

public class mindNoteList extends ListActivity
{
  private static final int ACTIVITY_EDIT = 1;
  private static final int DELETE_ID = 1;
  Bundle bl;
  private int flagNum;
  private MindNoteDB mDbHelper;
  private Button newNote;
  private String tagsKey;
  
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
    setContentView(R.layout.note_list);
    
    newNote = (Button)findViewById(R.id.Button_New);
           
    mDbHelper =new MindNoteDB(this);
    this.mDbHelper.open();   
    
    newNote.setOnClickListener(new OnClickListener() {
		
		public void onClick(View v) {
			// TODO Auto-generated method stub
			 Intent intent = new Intent();
             intent.setClass(mindNoteList.this, mindNoteEdit.class);
             startActivity(intent);
		}
	});
    
    bl = getIntent().getExtras();    
    flagNum = bl.getInt("flag");    
    tagsKey = bl.getString("tagsKey");    
    fillData();
    ListView localListView = getListView();
    registerForContextMenu(localListView);
	}

 	private void fillData()
 	{
 	    Cursor c = null;
 	    
 	    switch (this.flagNum) {
		case 0:
			// 查询全部数据，返回游标对象
	 		 c = mDbHelper.searchNotes(tagsKey);
			break;
		case 1: 	// 查询全部数据，返回游标对象
	 		 c = mDbHelper.fetchTagNotes(tagsKey);
			break;
		default:
			// 查询全部数据，返回游标对象
	 		 c = mDbHelper.fetchAllNotes();
			break;
		}
 	 // 让Activity开始管理游标对象
 		startManagingCursor(c);
 	    String[] from = new String[] { mDbHelper.NOTE_TITLE ,mDbHelper.NOTE_TIME,mDbHelper.NOTE_TAG};
 		int[] to = new int[] { R.id.textViewItem,R.id.textViewTime,R.id.textViewTag};
 	 // 创建一个SimpleCursorAdapter对象
 		SimpleCursorAdapter sca = new SimpleCursorAdapter(this, R.layout.notes_row, c, from, to);
 	// 给ListActivity设置Adapter
 	    setListAdapter(sca);
 	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		super.onCreateContextMenu(menu, v, menuInfo);
	    menu.add(0,DELETE_ID , 0, R.string.menu_delete);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
	      // 当删除按钮被选择时
		case DELETE_ID:
		// AdapterContextMenuInfo里包含ListView中选中项的信息
		 AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
	// 删除Id
		 mDbHelper.deleteNote(info.id);
		// 删除以后再次填充数据
		 fillData();
		return true;
		 }
		 return super.onContextItemSelected(item);

	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
	    Intent localIntent = new Intent(this, mindNoteEdit.class);
	    getListView().setSelected(true);
	    localIntent.putExtra("_id", id);
	    startActivityForResult(localIntent, 1);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
	    fillData();
	}
 	
}