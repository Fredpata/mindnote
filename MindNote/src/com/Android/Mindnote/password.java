package com.Android.Mindnote;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.security.MessageDigest;

public class password extends Activity
{
  public static final String KEY_passwordFlag_PRIVATE = "KEY_passwordFlag_PRIVATE";
  public static final String KEY_spAnswer_PRIVATE = "KEY_spAnswer_PRIVATE";
  public static final String KEY_spPassword_PRIVATE = "KEY_spPassword_PRIVATE";
  public static final String KEY_spQuestion_PRIVATE = "KEY_spQuestion_PRIVATE";
  public static final String passwordFlag_PRIVATE = "passwordFlag_PRIVATE";
  public static final String spAnswer_PRIVATE = "spAnswer_PRIVATE";
  public static final String spPassword_PRIVATE = "spPassword_PRIVATE";
  public static final String spQuestion_PRIVATE = "spQuestion_PRIVATE";
  private Button But_pass;
  private String answerTe;
  private String confirmPasswordTe;
  private int flagpass;
  private SharedPreferences pAnswersp;
  private SharedPreferences pQuestionsp;
  private SharedPreferences passwordFlagSP;
  private SharedPreferences passwordFlagSP1;
  private String passwordTe;
  private SharedPreferences passwordsp;
  private String questionTe;
  private EditText text_cofirmPassword;
  private EditText text_pAnswer;
  private EditText text_pQuestion;
  private EditText text_password;

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.password);
    text_password = (EditText)findViewById(R.id.passwordE);    
    text_cofirmPassword = (EditText)findViewById(R.id.cPasswordE);   
    text_pQuestion = (EditText)findViewById(R.id.questionE);    
    text_pAnswer = (EditText)findViewById(R.id.answerE);    
    But_pass = (Button)findViewById(R.id.PassButton1);    
    
    But_pass.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				savePassword();
				finish();
			}			
		});    
  }

//MD5加密，32位   
  public   static  String MD5(String str)  
  {  
      MessageDigest md5 = null ;  
      try   
      {  
          md5 = MessageDigest.getInstance("MD5" );  
      }catch (Exception e)  
      {  
          e.printStackTrace();  
          return   "" ;  
      }  
        
      char [] charArray = str.toCharArray();  
      byte [] byteArray =  new   byte [charArray.length];  
        
      for ( int  i =  0 ; i < charArray.length; i++)  
      {  
          byteArray[i] = (byte )charArray[i];  
      }  
      byte [] md5Bytes = md5.digest(byteArray);  
        
      StringBuffer hexValue = new  StringBuffer();  
      for (  int  i =  0 ; i < md5Bytes.length; i++)  
      {  
          int  val = (( int )md5Bytes[i])& 0xff ;  
          if (val <  16 )  
          {  
              hexValue.append("0" );  
          }  
          hexValue.append(Integer.toHexString(val));  
      }  
      return  hexValue.toString();  
  }  

  private void savePassword()
  {
    passwordTe = MD5(this.text_password.getText().toString());
    confirmPasswordTe = MD5(this.text_cofirmPassword.getText().toString());
    questionTe = this.text_pQuestion.getText().toString();
    answerTe = this.text_pAnswer.getText().toString();
    if (this.text_password.getText().toString().length() == 0)
      this.text_password.setError("密码不能为空");
    if (this.text_cofirmPassword.getText().toString().length() == 0)
      this.text_cofirmPassword.setError("密码不能为空");
    if (this.text_pQuestion.getText().toString().length() == 0)
      this.text_pQuestion.setError("请输入内容");
    if (this.text_pAnswer.getText().toString().length() == 0)
      this.text_pAnswer.setError("请输入内容");
    if ((this.text_password.getText().toString().length() == 0) || (this.text_cofirmPassword.getText().toString().length() == 0) || (this.questionTe.length() == 0) || (this.answerTe.length() == 0))
      Toast.makeText(this, "请输入内容", Toast.LENGTH_SHORT).show();
    else if(!(passwordTe.equals(confirmPasswordTe)))
    	Toast.makeText(this,"密码与密码（确认）必须保持一致",Toast.LENGTH_SHORT)
    	.show();
      passwordsp = getSharedPreferences("spPassword_PRIVATE", MODE_PRIVATE);
      pQuestionsp = getSharedPreferences("spQuestion_PRIVATE",MODE_PRIVATE);
      pAnswersp = getSharedPreferences("spAnswer_PRIVATE", MODE_PRIVATE);
      passwordFlagSP = getSharedPreferences("KEY_passwordFlag_PRIVATE", MODE_PRIVATE);
      SharedPreferences.Editor localEditor1 = this.passwordsp.edit();
      SharedPreferences.Editor localEditor2 = this.pQuestionsp.edit();
      SharedPreferences.Editor localEditor3 = this.pAnswersp.edit();
    
      localEditor1.putString("KEY_spPassword_PRIVATE", passwordTe);
      localEditor2.putString("KEY_spQuestion_PRIVATE", questionTe);
      localEditor3.putString("KEY_spAnswer_PRIVATE", answerTe);
      
      localEditor1.commit();
      localEditor2.commit();
      localEditor3.commit();
      SharedPreferences.Editor localEditor4 = this.passwordFlagSP.edit();
      localEditor4.putInt("KEY_passwordFlag_PRIVATE", 1);
      localEditor4.commit();
      passwordFlagSP = getSharedPreferences("KEY_passwordFlag_PRIVATE", MODE_PRIVATE);
      flagpass= passwordFlagSP.getInt("KEY_passwordFlag_PRIVATE", 0);
      
      if (this.flagpass == 1)
      {
      Intent localIntent = new Intent();
      localIntent.setClass(this, start.class);
      finish();
      startActivity(localIntent);}
    }
  }
