package com.Android.Mindnote;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.security.MessageDigest;

public class start extends Activity
{
  public static final String FIRST_RUN = "first";
  public static final String PREFS_NAME = "MyPrefsFile";
  private SharedPreferences passwordFlagSP;
  private String Password;
  private String ansText;
  private String answerText;
  private boolean first;
  private int flagPasswordNum;
  private Button ok;
  private SharedPreferences passwordsp;
  private EditText text_password,localEditText;
  private String tpassword;
  private View localView;

  
  protected void onCreate(Bundle paramBundle)
  {
	super.onCreate(paramBundle);
    setContentView(R.layout.start);
    ok = (Button)findViewById(R.id.stOK);    
    text_password = (EditText)findViewById(R.id.stPassword);    
    
    
    SharedPreferences localSharedPreferences2 = getSharedPreferences("MyPrefsFile", MODE_PRIVATE);
    SharedPreferences.Editor localEditor = localSharedPreferences2.edit();
    first = localSharedPreferences2.getBoolean(FIRST_RUN, true);
    
    if (first)
    {
      Toast.makeText(this, "第一次运行",Toast.LENGTH_SHORT).show();
      localEditor.putBoolean("first", false);
      localEditor.commit();
      Intent localIntent = new Intent();
      localIntent.setClass(this, password.class);
      startActivity(localIntent);
    }
    passwordFlagSP = getSharedPreferences(password.KEY_passwordFlag_PRIVATE,0 );
    
    flagPasswordNum = passwordFlagSP.getInt("KEY_passwordFlag_PRIVATE", 0);
    passwordsp = getSharedPreferences("spPassword_PRIVATE", MODE_PRIVATE);
    
    Password = passwordsp.getString("KEY_spPassword_PRIVATE", null);
    
    ok.setOnClickListener(new OnClickListener() {
		
		public void onClick(View v) {
			// TODO Auto-generated method stub
			tpassword =MD5(text_password.getText().toString());
			OK();
		}
	});
  }
  private void OK()
  {    
    if (!tpassword.equals(Password))
      Toast.makeText(this, "Password is wrong,Please try again.",Toast.LENGTH_SHORT).show();
    else
    {
      if (flagPasswordNum == 2)
      {
        Intent localIntent1 = new Intent();
        localIntent1.setClass(this, password.class);
        finish();
        startActivity(localIntent1);
      }else{
    	  Intent localIntent2 = new Intent();
          localIntent2.setClass(this, MindNoteActivity.class);
          finish();
          startActivity(localIntent2);
      }
      
    }
  }

 

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    paramMenu.add(0, 1, 0, "重置密码");
    return super.onCreateOptionsMenu(paramMenu);
  }
//MD5加密，32位   
  public   static  String MD5(String str)  
  {  
      MessageDigest md5 = null ;  
      try   
      {  
          md5 = MessageDigest.getInstance("MD5" );  
      }catch (Exception e)  
      {  
          e.printStackTrace();  
          return   "" ;  
      }  
        
      char [] charArray = str.toCharArray();  
      byte [] byteArray =  new   byte [charArray.length];  
        
      for ( int  i =  0 ; i < charArray.length; i++)  
      {  
          byteArray[i] = (byte )charArray[i];  
      }  
      byte [] md5Bytes = md5.digest(byteArray);  
        
      StringBuffer hexValue = new  StringBuffer();  
      for (  int  i =  0 ; i < md5Bytes.length; i++)  
      {  
          int  val = (( int )md5Bytes[i])& 0xff ;  
          if (val <  16 )  
          {  
              hexValue.append("0" );  
          }  
          hexValue.append(Integer.toHexString(val));  
      }  
      return  hexValue.toString();  
  }  

  public boolean onMenuItemSelected(int paramInt, MenuItem paramMenuItem)
  {
    SharedPreferences.Editor localEditor = this.passwordFlagSP.edit();
    localEditor.putInt("KEY_passwordFlag_PRIVATE", 3);
    localEditor.commit();
    String str1 = getSharedPreferences("spQuestion_PRIVATE", MODE_PRIVATE).getString("KEY_spQuestion_PRIVATE", null);
    answerText = getSharedPreferences("spAnswer_PRIVATE", MODE_PRIVATE).getString("KEY_spAnswer_PRIVATE", null);
    switch (paramMenuItem.getItemId()) {
	case 1:
		localView = LayoutInflater.from(this).inflate(R.layout.question, null);
	    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
	    localBuilder.setTitle("重置密码");
	    localBuilder.setMessage(str1);
	    localBuilder.setView(localView);	
	    
	    localBuilder.setPositiveButton("确认",new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub		   
				localEditText = (EditText)localView.findViewById(R.id.answerTextRE);
			    ansText = localEditText.getText().toString();
			    if (ansText.equals(answerText))
			    {
			      Intent localIntent = new Intent();
			      
			      localIntent.setClass(start.this, password.class);
			      startActivity(localIntent);
			    }
			    else
			    {			      
			     Toast.makeText(start.this, "答案错误",Toast.LENGTH_SHORT).show();
			    }
			} 
	    	
	    });
	    localBuilder.show();
		break;
    }
    return super.onMenuItemSelected(paramInt, paramMenuItem);
  }
  

}