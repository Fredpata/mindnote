package com.Android.Mindnote;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class tags extends Activity
{
  Bundle bl;
  private EditText tagsInput;
  private String tagsKey;
  private Button tagsOK;

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.tags);
    tagsOK = (Button)findViewById(R.id.TagsOK);   
    tagsInput = (EditText)findViewById(R.id.TagsInput);
        
    tagsOK.setOnClickListener(new View.OnClickListener() {

		public void onClick(View v) {
			// TODO Auto-generated method stub
			tagsKey = tagsInput.getText().toString();			
			Intent localIntent=new Intent();			 
		    Bundle bl =getIntent().getExtras();		   	 
		    bl.putString("tagsKey", tagsKey);		    
		    localIntent.putExtras(bl);	 		    
		    localIntent.setClass(tags.this, mindNoteList.class);
		    startActivity(localIntent);
		}			
	});
    
  }
}