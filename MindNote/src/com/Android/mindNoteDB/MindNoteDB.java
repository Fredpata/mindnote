package com.Android.mindNoteDB;

import java.sql.Date;
import java.text.SimpleDateFormat;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MindNoteDB {
	
	public static final String NOTE_TITLE= "title";
	public static final String NOTE_TIME= "time";
	public static final String NOTE_TAG= "tag";
	public static final String NOTE_CONTENT= "content";
	public static final String KEY_ROWID= "_id";
	
	private static final String TAG = "MindNoteDbAdapter";
	private DatabaseHelper mDbHelper;
	private SQLiteDatabase mDb;
	
	private static final String DATABASE_NAME = "MindNote";
	private static final String DATABASE_TABLE="notes";
	private static final int DATABASE_VERSION= 1;  //2??
	
	private static final  String  DATABASE_CREATE="create table "+ DATABASE_TABLE + 
			"(_id integer primary key autoincrement,title text not null,content text not null,time  text not null ,tag text not null );" ;
		
	
	private final Context mCtx;
			
	private class DatabaseHelper extends SQLiteOpenHelper{

		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			System.out.println("Table before Create");
			db.execSQL(DATABASE_CREATE);
			System.out.println("Table after Create");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			//把版本更新计入日志
			Log.w(TAG, "Upgrading database from version"+oldVersion+"to"
					+newVersion+",which will destore all old data");
			//更新现存的数据库来保证是最新的版本
			//通过比较oldversion和newversion值来处理多个以前的版本
			//创建新表……有待改进！！！！！！！
			onCreate(db);			
		}		
	}
	
	public MindNoteDB(Context ctx){
		this.mCtx = ctx;
	}
	
	public MindNoteDB open() throws SQLException{
		mDbHelper = new DatabaseHelper(mCtx);
		try {
			mDb = mDbHelper.getWritableDatabase();
		} catch (SQLException ex) {
			// TODO: handle exception
			mDb = mDbHelper.getWritableDatabase();
		}
		
		return this;
	}
	
	public void close(){
		mDbHelper.close();
	}
	
	public  long createNote(String title, String content,String tag){
		//在SQLite数据库中新建文档
		
	//获取当前系统时间
			Date localDate = new Date(System.currentTimeMillis());
	//将系统时间格式化为 周-年-月-日-时-分，同时保存到变量str中 
	    String str = new SimpleDateFormat("EE yyyy-MM-dd  HH:mm").format(localDate);
			ContentValues initialValues = new ContentValues();
	//将在程序文档编辑页面的相关数据提交
			initialValues.put(NOTE_TITLE, title);
			initialValues.put(NOTE_CONTENT, content);
			initialValues.put(NOTE_TIME, str);
			initialValues.put(NOTE_TAG, tag);
			
			return mDb.insert(DATABASE_TABLE, null, initialValues);

	}
	
	public boolean deleteNote(long rowId){
		return mDb.delete(DATABASE_TABLE, KEY_ROWID+"="+rowId, null)>0;
	}
	
	public Cursor fetchNote(long rowId)throws SQLException{
		Cursor mCursor = 
				
				mDb.query(true, DATABASE_TABLE, new String[]{KEY_ROWID,
						NOTE_TITLE,NOTE_CONTENT},KEY_ROWID+"="+rowId, null	, null, null,  null, null);
				//mDb.query(distinct, table, columns, selection, selectionArgs, groupBy, having, orderBy, limit)
		if(mCursor != null){
			mCursor.moveToFirst();
		}
		return mCursor;
	}
	
	//返回对应标签的结果
	public Cursor fetchTagNotes(String Tags) {
		return mDb.query(DATABASE_TABLE, new String[] {KEY_ROWID, NOTE_TITLE,NOTE_CONTENT,NOTE_TIME,NOTE_TAG}, NOTE_TAG+" like "+"'"+Tags+"'",null, null, null, null,null);
	}

	public Cursor searchNotes(String Tags) {
		return mDb.query(DATABASE_TABLE, new String[] {KEY_ROWID, NOTE_TITLE,NOTE_CONTENT,NOTE_TIME,NOTE_TAG}, NOTE_TITLE+" like "+"'%"+Tags+"%'",null, null, null, null,null);
	}

	public Cursor fetchAllNotes() {
		// TODO Auto-generated method stub
		return mDb.query(DATABASE_TABLE, new String[] {KEY_ROWID, NOTE_TITLE,NOTE_CONTENT,NOTE_TIME,NOTE_TAG}, null, null, null, null, null);

	}
	public boolean updateNote(long rowId,String title,String Content,String tag){
		Date localDate = new Date(System.currentTimeMillis());
		//更新时，再次获取系统时间
	    String str = new SimpleDateFormat("EE yyyy-MM-dd  HH:mm").format(localDate);
		//格式化获取的系统时间
		ContentValues args = new ContentValues();
		args.put(NOTE_TITLE, title);
		args.put(NOTE_CONTENT,Content );
		args.put(NOTE_TIME, str);
		args.put(NOTE_TAG, tag);

		
		return mDb.update(DATABASE_TABLE, args, KEY_ROWID+"="+rowId, null)>0;
		//return mDb.update(table, values, whereClause, whereArgs)
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
